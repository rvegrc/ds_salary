from fastapi import FastAPI
from pydantic import BaseModel

import pandas as pd
import numpy as np
import sklearn


# import fird-party libraries and modules

import json

from pprint import pprint

import xgboost as xgb

from sklearn.pipeline import Pipeline as skl_pipeline

from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder, PowerTransformer

from sklearn.compose import ColumnTransformer

from typing import Dict, Any, List

import warnings

import joblib

# output format
pd.set_option('display.float_format', '{:.3f}'.format)
sklearn.set_config(transform_output='pandas')
warnings.filterwarnings("ignore")

# constant
RAND_ST = 345


model = joblib.load('tmp_model_oe.joblib')


app = FastAPI()

@app.get("/")
async def root():
    return {"message": "DS Salary API"}


class XInputDict(BaseModel):
    experience_level: Dict[str, Any]
    employment_type: Dict[str, Any]
    job_title: Dict[str, Any]
    salary_currency: Dict[str, Any]
    employee_residence: Dict[str, Any]
    remote_ratio: Dict[str, Any]
    company_location: Dict[str, Any]
    company_size: Dict[str, Any]
    inflation: Dict[str, Any]
    usd_rate: Dict[str, Any]


class OutputDict(BaseModel):
    index: List[int]
    y_pred: List[float] 

@app.post("/predict")
async def predict(data: XInputDict):
    '''Predict salary'''
    # convert to dataframe
    tmp = data.model_dump_json()
    df = pd.read_json(tmp)
    # pprint(df)
    # predict
    y_pred = model.predict(df)
    # pprint(list(y_pred))
    # pprint(list(df.index))
    dout = OutputDict(index=list(df.index), y_pred=list(y_pred))
    return dout