FROM python:3.11-slim

WORKDIR /app

COPY tmp_model_oe.joblib .

COPY ds_salary_api.py .

COPY requirements.txt .

RUN pip3 install -r requirements.txt

ENTRYPOINT [ "uvicorn", "ds_salary_api:app", "--host", "0.0.0.0", "--port", "8080"]